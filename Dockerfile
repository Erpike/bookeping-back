# FROM -  из какого образа сделать контейнер
# FROM <image>:<tag>
# Docker hub - хранилище официальных образов
# Alpine - в режиме разработке не используется (преимущество: компактный)
FROM python:3.7

# set work directory
WORKDIR /web/app

# set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

ENV ANGULAR_THREAD 'http://127.0.0.1:4200'

# RUN - команда докера, которая выполняет команду внутри контейнера
RUN pip install pipenv

# COPY - copy files from directory to container
# COPY <file1>  [<fileN>] <target_dir>
COPY ./Pipfile ./Pipfile.lock /web/app/

# TODO: Read about pipenv flags
# --system:
# --deploy:
# --ignore-pipfile:
RUN pipenv install --system --deploy --ignore-pipfile

# CMD
# нужно задавать иначе контейнер после такого как сбилдится - схлопнется
