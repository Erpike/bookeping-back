from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('admin/', admin.site.urls),
    path(r'api-auth/', include('bukepin.api.urls')),

]
