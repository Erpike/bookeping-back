from django.urls import path

from .views import AuthView, ExampleView

urlpatterns = [
    path(r'test/', ExampleView.as_view()),
    path(r'auth/', AuthView.as_view()),
]
