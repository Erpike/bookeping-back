from django.contrib.auth.models import User

from rest_framework import status
from rest_framework.authentication import BasicAuthentication, SessionAuthentication, TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response
from rest_framework.views import APIView


class ExampleView(APIView):
    authentication_classes = (TokenAuthentication, SessionAuthentication)
    permission_classes = (IsAuthenticated,)

    def get(self, request):
        return Response('hello')

    def post(self, request):
        return Response('success')



class AuthView(APIView):
    authentication_classes = (BasicAuthentication,)
    permission_classes = (AllowAny,)

    def get_object(self):
        user = User.objects.filter(username=self.request.data.get('username')).first()
        if user and user.check_password(self.request.data.get('password')):
            return user
        return None

    def post(self, request):
        user = self.get_object()
        if not user:
            return Response({'message': 'access denied'}, status=status.HTTP_401_UNAUTHORIZED)
        token, created = Token.objects.get_or_create(user=user)
        token = token.key
        response = {
            'user': user.username,
            'token': token
        }
        return Response(response, status=status.HTTP_200_OK)
